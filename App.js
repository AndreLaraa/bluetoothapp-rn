/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import { View, Text, NativeModules, TouchableOpacity, NativeEventEmitter } from 'react-native';

const { BLMessagesWrapper } = NativeModules;
const { BMMessagesEmitter } = NativeModules;

const BMMessageEvents = new NativeEventEmitter(BMMessagesEmitter);

export default class App extends Component {
  constructor() {
    super();
    BMMessageEvents.addListener("updateAgent", (_data, _) => {
      console.log("chegou listener updateAgent");
      console.log(_data);
      alert("updateAgent "+_data);
    }
    );

    BMMessageEvents.addListener("didConnectedTo", (_data, _) => {
      console.log("chegou listener didConnectedTo");
      console.log(_data);
      alert("didDisconnectTo "+String(_data));
    }
    );

    BMMessageEvents.addListener("didReceiveData", (_data, _) => {
      console.log("chegou listener didReceiveData");
      console.log(_data);

      alert("didReceiveData "+JSON.stringify(_data));
    }
    );

    BMMessageEvents.addListener("didWriteData", (_data, _) => {
      console.log("chegou listener didWriteData");
      console.log(_data);

      alert("didWriteData "+String(_data));
    }
    );
  }
  
  render() {
    return (
      <View
        style={{
          marginTop: 100,
          height: 200,
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
      }}>
        <TouchableOpacity
          onPress={() => {
            BLMessagesWrapper.initializeBLWrapper();

          }}>
          <Text>Procurar Devices</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            BLMessagesWrapper.conectarAoPrimeiro();

          }}>
          <Text>Conectar ao primeiro</Text>
        </TouchableOpacity>

        <TouchableOpacity
        
          onPress={() => {
            BLMessagesWrapper.enviarMensagem();

          }}>
          <Text>Enviar "Hello World"</Text>
        </TouchableOpacity>

        <TouchableOpacity
        
          onPress={() => {

            BLMessagesWrapper.listAgents((error, agents)  => {
              console.log("quantidade de agentes: " + JSON.stringify(agents));
              alert("agentes:  "+JSON.stringify(agents));
            });
            

          }}>
          <Text>Listar agentes</Text>
        </TouchableOpacity>


        <TouchableOpacity
        
        onPress={() => {
          BLMessagesWrapper.connectToIndex(0);

        }}>
        <Text>Conectar ao Index</Text>
        </TouchableOpacity>

        <TouchableOpacity
        
        onPress={() => {
          BLMessagesWrapper.sendMessageToIndex(0,"ABC");

        }}>
        <Text>Enviar string para Index</Text>
      </TouchableOpacity>
      </View>
    );
  }
}