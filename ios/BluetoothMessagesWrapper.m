//
//  BluetoothMessagesWrapper.m
//  BMMessagesReact
//
//

#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"
#import "React/RCTEventEmitter.h"

@interface RCT_EXTERN_MODULE(BLMessagesWrapper, NSObject)
RCT_EXTERN_METHOD(initializeBLWrapper)
RCT_EXTERN_METHOD(listAgents: (RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(conectarAoPrimeiro)
RCT_EXTERN_METHOD(enviarMensagem)

RCT_EXTERN_METHOD(sendMessageToUUID: (NSString*)uuid message:(NSString*)message)
RCT_EXTERN_METHOD(sendMessageToIndex: (NSInteger)index message:(NSString*)message)

RCT_EXTERN_METHOD(connectToUUID: (NSString*)uuid)
RCT_EXTERN_METHOD(connectToIndex: (NSInteger)index)

@end

@interface RCT_EXTERN_MODULE(BMMessagesEmitter, RCTEventEmitter)
RCT_EXTERN_METHOD(supportedEvents)

@end
