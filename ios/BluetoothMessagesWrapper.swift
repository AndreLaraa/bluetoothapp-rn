//
//  BluetoothMessagesWrapper.swift
//  BMMessagesReact
//
//

import BluetoothMessages
import React

enum RCTEvent : String, CaseIterable {
  case updateAgent = "updateAgent"
  case didConnectedTo = "didConnectedTo"
  case didDisconnectTo = "didDisconnectTo"
  case didReceiveData = "didReceiveData"
  case didWriteData = "didWriteData"
}

@objc(BLMessagesWrapper)
class BLMessagesWrapper : NSObject {
  
  private var BAMmanager : BluetoothMessages.BluetoothAgentManager!
  private var RCTEmitter = BMMessagesEmitter()
  private var new_agent_callback : RCTResponseSenderBlock?
  private var agents = [BMBluetoothAgent]()
  private var _retrieveAgents : Timer!
  
  @objc
  static func requiresMainQueueSetup() -> Bool {
    return true
  }
  
  @objc
  func initializeBLWrapper() {
    print("chegou: initializeBLWrapper")
    BAMmanager = BluetoothAgentManager(delegate: self)
    BAMmanager.BMConnectionDelegate = self
    
    _retrieveAgents = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(updateAgents), userInfo: nil, repeats: true)
  }
  
  @objc private func updateAgents(){
    agents = BAMmanager.returnAllAgents()
  }
  
  @objc
  func conectarAoPrimeiro(){
//    if let ag = agents.first(where: { $0.uuid == ""}) {
    if let first = agents.first {
      BAMmanager.BMConectToAgent(agent: first)
    }
  }
  
  @objc
  func connectToUUID(_ uuid: String){
    if let first = agents.first(where: { $0.uuid == uuid}) {
      BAMmanager.BMConectToAgent(agent: first)
    }
  }
  
  @objc
  func connectToIndex(_ index: Int){
    if index >= agents.count { return }
    BAMmanager.BMConectToAgent(agent: agents[index])
  }
  
  @objc
  func sendMessageToUUID(_ uuid: String, message: String){
    if let first = agents.first(where: { $0.uuid == uuid}) {
      let data = message.data(using: .utf8)!
      
      var _message = BMMessage()
      _message.id = Int(Date().timeIntervalSince1970)
      _message.content = data
      
      BAMmanager.BMSendMessageTo(first, _message)
    }
  }
  
  @objc
  func sendMessageToIndex(_ index: Int, message: String){
    if index >= agents.count { return }
    
    let data = message.data(using: .utf8)!
    
    var _message = BMMessage()
    _message.id = Int(Date().timeIntervalSince1970)
    _message.content = data
    
    BAMmanager.BMSendMessageTo(agents[index], _message)
  }
  
  @objc(listAgents:)
  func listAgents(_ callback: RCTResponseSenderBlock) {
    var dict = [String:String]()
    for ag in agents {
      dict.updateValue(ag.description, forKey: ag.uuid)
    }
    callback([NSNull() ,dict])
  }
  
  @objc
  func enviarMensagem(){
    if let first = agents.first {
      let messageString = "Hello World"
      let data = messageString.data(using: .utf8)!
      
      var message = BMMessage()
      message.id = Int(Date().timeIntervalSince1970)
      message.content = data // pode ser int, string, qualquer estrutura/tipo de dado
      
      BAMmanager.BMSendMessageTo(first, message)
    }
  }
  
}

// Agent is connecting to someone by itself
extension BLMessagesWrapper : BMBluetoothAgentDelegate {
  func updateAgent(agent: BMBluetoothAgent, status: BMAgentStatus) {
    agents.append(agent)
    BMMessagesEmitter.shared!.sendEventToRCT(event: RCTEvent.updateAgent, data: agent)
  }
  
  func didConnectedTo(agent: BMBluetoothAgent) {
    BMMessagesEmitter.shared!.sendEventToRCT(event: RCTEvent.didConnectedTo, data: agent)
  }
}

// Agent is being connected by someone
extension BLMessagesWrapper : BMBluetoothConectionDelegate {
  func didConnectTo(agent: BMBluetoothAgent) {
    BMMessagesEmitter.shared!.sendEventToRCT(event: RCTEvent.didConnectedTo, data: agent)
  }
  
  func didDisconnectTo(agent: BMBluetoothAgent) {
    BMMessagesEmitter.shared!.sendEventToRCT(event: RCTEvent.didDisconnectTo, data: agent)
  }
  
  func didReceiveData(data: Data?, error: Error?) {
    let _data : Any = error == nil ? data! : error!
    BMMessagesEmitter.shared!.sendEventToRCT(event: RCTEvent.didReceiveData, data: _data)
  }
  
  func didWriteData(error: Error?) {
    let _data : Any = error == nil ? true : false
    BMMessagesEmitter.shared!.sendEventToRCT(event: RCTEvent.didWriteData, data: _data)
  }
  
}

@objc(BMMessagesEmitter)
class BMMessagesEmitter : RCTEventEmitter {
  
  public static var shared : BMMessagesEmitter?
  
  override init() {
    super.init()
    BMMessagesEmitter.shared = self
  }
  
  func sendEventToRCT(event: RCTEvent, data: Any){
    var body = [String:Any]()
    
    switch event {
    case .updateAgent:
      body["description"] = (data as! BMBluetoothAgent).description
    case .didConnectedTo:
      body["description"] = (data as! BMBluetoothAgent).description
    case .didDisconnectTo:
      body["description"] = (data as! BMBluetoothAgent).description
    case .didReceiveData:
      body["description"] = ((data as? Data) != nil) ? String(decoding:  (data as! Data), as: UTF8.self) : false
    case .didWriteData:
      body["description"] = (data as! Bool)
    }
    
    sendEvent(withName: event.rawValue, body: body)
  }
  
  override func supportedEvents() -> [String]! {
    let cases : [String] = RCTEvent.allCases.map{$0.rawValue}
    return cases
  }
}

